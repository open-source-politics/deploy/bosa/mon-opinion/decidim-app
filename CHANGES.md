# alt/bosa custom changes

## E-ID Verified badge
- `app/helpers/decidim/badge_helper.rb`
- `app/views/decidim/admin/officializations/index.html.erb`
- `lib/extends/cells/decidim/author_cell_extend.rb`
- `lib/extends/models/decidim/user_extend.rb`
- `lib/extends/presenters/decidim/user_presenter_extend.rb`
- `lib/extends/presenters/decidim/proposals/official_author_presenter_extend.rb`
